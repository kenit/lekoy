package services

import (
	"context"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"sync"
)

const path = "/etc/wireguard/"

type DummyWG struct {
	mutex sync.Mutex
}

type SendFileArgs struct {
	Name string
	Data []byte
}

type SendFileReply struct {
	Status  string
	Message string
}

func (d *DummyWG) SendFile(ctx context.Context, args *SendFileArgs, reply *SendFileReply) error {
	d.mutex.Lock()
	defer d.mutex.Unlock()
	fullName := path + args.Name + ".conf"
	err := ioutil.WriteFile(fullName, args.Data, os.ModePerm)
	if err != nil {
		log.Println("Unable to create file:", err)
		return err
	}
	reply.Status = "File write success"
	reply.Message = "File save: " + fullName
	return nil
}

func (d *DummyWG) Up(ctx context.Context, args *SendFileArgs, reply *SendFileReply) error {
	fullName := path + args.Name + ".conf"
	err := command("up", fullName)
	if err != nil {
		return err
	}
	reply.Status = "Interface success UP"
	return nil
}

func (d *DummyWG) Down(ctx context.Context, args *SendFileArgs, reply *SendFileReply) error {
	fullName := path + args.Name + ".conf"
	err := command("down", fullName)
	if err != nil {
		return err
	}
	reply.Status = "Interface success DOWN"
	return nil
}

func command(command, filepath string) error {
	cmd := exec.Command("wg-quick", command, filepath)
	return cmd.Run()
}
