//package main
//

package main

import (
	"gitlab.com/kenit/lekoy/cli/cmd"
)

func main() {
	cmd.Execute()
}