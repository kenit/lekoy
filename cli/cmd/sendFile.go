package cmd

import (
	"context"
	"github.com/spf13/cobra"
	"gitlab.com/kenit/lekoy/services"
	"io/ioutil"
	"log"
)

var sendCmd = &cobra.Command{
	Use:   "send",
	Short: "send config to server",

	Run: runSend,
}

func runSend(cmd *cobra.Command, args []string) {
	serverAddr := cmd.Flag("server").Value.String()
	client := newClient(serverAddr)
	defer client.Close()
	name := cmd.Flag("name").Value.String()
	file, _ := ioutil.ReadFile(name + ".conf")

	request := services.SendFileArgs{
		Name: name,
		Data: file}
	replay := services.SendFileReply{}
	client.Call(
		context.Background(),
		"SendFile",
		request,
		&replay)
	log.Printf("%v", replay)
}
