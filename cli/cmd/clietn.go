package cmd

import (
	"crypto/tls"
	"github.com/smallnest/rpcx/v6/protocol"

	"github.com/smallnest/rpcx/v6/client"
)



func newClient(addr string) client.XClient {

	d := client.NewPeer2PeerDiscovery("quic@"+addr, "")
	opt := client.DefaultOption
	opt.SerializeType = protocol.JSON
	opt.TLSConfig = &tls.Config{
		InsecureSkipVerify: true,
	}
	return client.NewXClient("Arith", client.Failtry, client.RandomSelect, d, opt)

}