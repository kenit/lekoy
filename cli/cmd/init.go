package cmd
func init() {

	rootCmd.PersistentFlags().StringP("server", "s", "localhost:3000", "address of server")
	rootCmd.MarkPersistentFlagRequired("server")
	rootCmd.PersistentFlags().String("name","","name of .config file")
	rootCmd.MarkPersistentFlagRequired("name")

	rootCmd.AddCommand(sendCmd)
	rootCmd.AddCommand(upCmd)
	rootCmd.AddCommand(downCmd)
}
