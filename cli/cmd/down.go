package cmd

import (
	"context"
	"github.com/spf13/cobra"
	"gitlab.com/kenit/lekoy/services"
	"log"
)

var downCmd = &cobra.Command{
	Use:   "down",
	Short: "down interface",
	Run:   runDown,
}

func runDown(cmd *cobra.Command, args []string) {
	serverAddr := cmd.Flag("server").Value.String()
	client := newClient(serverAddr)
	defer client.Close()
	replay := services.SendFileReply{}
	client.Call(
		context.Background(),
		"Down",
		services.SendFileArgs{Name: cmd.Flag("name").Value.String()},
		&replay)
	log.Printf("%v", replay)
}
