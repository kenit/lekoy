package cmd

import (
	"context"
	"github.com/spf13/cobra"
	"gitlab.com/kenit/lekoy/services"
	"log"
)

var upCmd = &cobra.Command{
	Use:   "up",
	Short: "up interface",
	Run: runUp,
}

func runUp(cmd *cobra.Command, args []string) {

	serverAddr := cmd.Flag("server").Value.String()
	client := newClient(serverAddr)
	defer client.Close()
	replay := services.SendFileReply{}
	client.Call(
		context.Background(),
		"Up",
		services.SendFileArgs{Name: cmd.Flag("name").Value.String()},
		&replay)
	log.Printf("%v", replay)
}
