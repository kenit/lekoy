//go run -tags quic server.go
package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"encoding/pem"
	"github.com/smallnest/rpcx/v6/server"
	"gitlab.com/kenit/lekoy/services"
	"math/big"
	"time"
)

func main() {

	// cert, err := tls.LoadX509KeyPair("server.pem", "server.key")

	//if err != nil {
	//	log.Print(err)
	//	return
	//}

	config := generateTLSConfig()

	s := server.NewServer(server.WithTLSConfig(config))
	s.RegisterName("Arith", new(services.DummyWG), "")

	err := s.Serve("quic", ":3000")
	if err != nil {
		panic(err)
	}
}

//func generateTLSConfig() (*tls.Config, error) {
//	key, err := rsa.GenerateKey(rand.Reader, 2048)
//	if err != nil {
//		return nil, err
//	}
//	template := x509.Certificate{
//		SerialNumber: big.NewInt(1),
//		NotBefore:    time.Now(),
//		NotAfter:     time.Now().Add(time.Hour),
//		KeyUsage:     x509.KeyUsageCertSign | x509.KeyUsageDigitalSignature,
//	}
//	certDER, err := x509.CreateCertificate(rand.Reader, &template, &template, &key.PublicKey, key)
//	if err != nil {
//		fmt.Println(err)
//		return nil, err
//	}
//	keyPEM := pem.EncodeToMemory(&pem.Block{
//		Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(key),
//	})
//	b := pem.Block{Type: "CERTIFICATE", Bytes: certDER}
//	certPEM := pem.EncodeToMemory(&b)
//
//	tlsCert, err := tls.X509KeyPair(certPEM, keyPEM)
//	if err != nil {
//		return nil, err
//	}
//
//	return &tls.Config{
//		Certificates: []tls.Certificate{tlsCert},
//	}, nil
//}
func generateTLSConfig() *tls.Config {
	key, err := rsa.GenerateKey(rand.Reader, 1024)
	if err != nil {
		panic(err)
	}
	//template := x509.Certificate{SerialNumber: big.NewInt(1)}
	template := x509.Certificate{
		SerialNumber: big.NewInt(1),
		NotBefore:    time.Now(),
		NotAfter:     time.Now().Add(time.Hour*24*365),
		KeyUsage:     x509.KeyUsageCertSign | x509.KeyUsageDigitalSignature,
	}
	certDER, err := x509.CreateCertificate(rand.Reader, &template, &template, &key.PublicKey, key)
	if err != nil {
		panic(err)
	}
	keyPEM := pem.EncodeToMemory(&pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(key)})
	certPEM := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: certDER})

	tlsCert, err := tls.X509KeyPair(certPEM, keyPEM)
	if err != nil {
		panic(err)
	}
	return &tls.Config{
		Certificates: []tls.Certificate{tlsCert},
	}
}
