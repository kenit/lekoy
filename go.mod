module gitlab.com/kenit/lekoy

go 1.15

replace (
	github.com/lucas-clemente/quic-go => github.com/lucas-clemente/quic-go v0.19.3
	google.golang.org/grpc => google.golang.org/grpc v1.29.0
)

require (
	github.com/smallnest/rpcx/v6 v6.0.0
	github.com/spf13/cobra v1.1.1
)
